import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/Login.vue'
import Home from '@/components/Home.vue'
import Welcome from '@/components/Welcome.vue'
import Users from '@/components/Users/Users.vue'
import Rights from '@/components/Rights/Rights.vue'
import Roles from '@/components/Rights/Roles.vue'
import Cat from '@/components/Goods/Cat.vue'
import Params from '@/components/Goods/Params.vue'
import List from '@/components/Goods/List.vue'
import Add from '@/components/Goods/Add.vue'
import Edit from '@/components/Goods/Edit.vue'
import Order from '@/components/Order/Order.vue'
import Report from '@/components/Report/Report.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/users', component: Users },
      { path: '/rights', component: Rights },
      { path: '/roles', component: Roles },
      { path: '/categories', component: Cat },
      { path: '/params', component: Params },
      { path: '/goods', component: List },
      { path: '/goods/add', component: Add },
      { path: '/goods/edit', component: Edit },
      { path: '/orders', component: Order },
      { path: '/reports', component: Report }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 配置路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  const token = window.sessionStorage.getItem('token')
  if (!token) return next('/login')
  next()
})

export default router
